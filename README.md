# sublimetext3_hightlight_for_aardio

#### 介绍
sublimetext3_hightlight_for_aardio

`Sublime Text 3` 编辑器的 `aardio` 语言高亮.


#### 安装教程

1. 复制 `aardio-syntax-highlight` 目录到 `{ST3}\Data\Packages\User\`目录下即可.

#### 使用说明

1. 重启ST3后再试.

#### 语法参考

https://www.sublimetext.com/docs/3/syntax.html

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

#### 感谢

[@xwysjzhg](https://gitee.com/xwysjzhg/st3) 的 st3 项目.